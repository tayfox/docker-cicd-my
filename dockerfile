FROM python:3
WORKDIR /code 
COPY . /code/ 
RUN pip install -r requirements.txt 
EXPOSE 8080
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8080
